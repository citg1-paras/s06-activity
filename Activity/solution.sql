-- a. List the books Authored by Marjorie Green. --
SELECT title FROM TITLE.title
INNER JOIN AUTHOR_TITLE ON TITLE.title_id = AUTHOR_TITLE.title_id
INNER JOIN AUTHOR ON AUTHOR_TITLE.au_id = AUTHOR.au_id
WHERE AUTHOR.au_id = 213-46-8915;

-- b. List the books Authored by Michael O'Leary. --
SELECT title FROM TITLE.title
INNER JOIN AUTHOR_TITLE ON TITLE.title_id = AUTHOR_TITLE.title_id
INNER JOIN AUTHOR ON AUTHOR_TITLE.au_id = AUTHOR.au_id
WHERE AUTHOR.au_id = 267-41-2394;

-- c. Write the author/s of "The Busy Executives Database Guide". --
SELECT AUTHOR.au_fname, AUTHOR.au_lname from AUTHOR 
INNER JOIN AUTHOR_TITLE ON AUTHOR.au_id = AUTHOR_TITLE.au_id 
INNER JOIN TITLE ON AUTHOR_TITLE.title_id = TITLE.title_id
WHERE title_id = BU1032;

-- d. Identify the publisher of "But Is It User Friendly?". --
SELECT PUBLISHER.pub_name from PUBLISHER
INNER JOIN TITLE ON PUBLISHER.pub_id = TITLE.pub_id
WHERE title_id = PC1035;

-- e. List the books published by Algodata Infosystems.--

SELECT title from TITLE.title
INNER JOIN PUBLISHER ON TITLE.pub_id = PUBLISHER.pub_id
WHERE PUBLISHER.pub_id = 1389;


-- DATABASE: blog_db --
CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(300) NOT NULL,
	datetime_create DATETIME NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_author_id
	FOREIGN KEY(author_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id
	FOREIGN KEY(post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_user_id
	FOREIGN KEY(user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id
	FOREIGN KEY(post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_user_id
	FOREIGN KEY(user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);